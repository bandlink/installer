#
#   Copyright 2021 Huawei Technologies Co., Ltd.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

---

- name: Create Directory for NFS
  file:
    path: "{{ NFS_PATH }}"
    state: directory
    mode: 0755

- name: Install NFS Kernel Server
  shell: "dpkg -i -G -E {{ NFS_DEB_PATH }}/nfs-kernel-server_1%3a1.3.4-2.1ubuntu5.3_{{ ARCH }}.deb"

- name: Edit NFS /etc/exports File
  lineinfile:
    path: /etc/exports
    line: "{{ NFS_PATH }} *(rw,no_root_squash,sync)"

- name: Restart NFS Server
  service:
    name: nfs-kernel-server.service
    state: restarted

- name: Load NFS Images
  shell: "docker load --input {{ NFS_IMAGE }}"

- name: Install NFS Client Provisioner For AMD64
  shell: |
    helm install --wait nfs-client-provisioner {{ NFS_CHART_FILE }} \
    --set nfs.server={{ groups.master[0] }} \
    --set nfs.path={{ NFS_PATH }} \
    --set image.repository={{ nfs_image_repository_amd64 }} \
    --set image.tag={{ nfs_image_tag_amd64 }}
  when: ARCH == "amd64"

- name: Install NFS Client Provisioner For ARM64
  shell: |
    helm install --wait nfs-client-provisioner {{ NFS_CHART_FILE }} \
    --set nfs.server={{ groups.master[0] }} \
    --set nfs.path={{ NFS_PATH }} \
    --set image.repository={{ nfs_image_repository_arm64 }} \
    --set image.tag={{ nfs_image_tag_arm64 }}
  when: ARCH == "arm64"

- name: Get Appstore Packages Data for All Worker Nodes
  set_fact:
    worker_appstore_str: "{{ groups.worker|join('(rw,sync,no_root_squash,no_subtree_check) ') }}(rw,sync,no_root_squash,no_subtree_check)"
  when: NODE_MODE == "muno"

- name: Add All IPs of Worker Nodes into /etc/exports File
  lineinfile:
    path: /etc/exports
    line: "{{ APPSTORE_PACKAGES }} {{ worker_appstore_str }}"
  when: NODE_MODE == "muno"

- name: Apply New File Mounts
  shell: exportfs -a
  when: NODE_MODE == "muno"
